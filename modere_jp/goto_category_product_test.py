# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from base_handler import BaseTest
import unittest
import time


class GotoCategoryProduct(BaseTest):

    def test_category_product(self):
        driver = self.driver
        driver.get(self.base_url+"/Category/personal-care")
        self.select_product()
        self.success = True
        time.sleep(5)

    def success_output(self):
        return  "Open Product detail Page from Category - PASS"
    
    def errorr_output(self):
        return  "Open Product detail Page from Category - FAIL"


if __name__ == '__main__':
    unittest.main()
