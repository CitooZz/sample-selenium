# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from base_handler import BaseTest
import unittest
import time


class TestCartDetailPageAction(BaseTest):
    """
    this test case do:
    - add product to cart
    - change quantity
    - delete product
    - add another product
    - click cart checkout button
    """

    def test_cart_detail_action(self):
        driver = self.driver
        driver.get(self.base_url)
        self.login()
        time.sleep(3)
        self.goto_category("パーソナルケア")
        elem = driver.find_elements_by_class_name("product-image")
        product_1 = elem[0]
        product_1.click()
        product_1.click()
        time.sleep(4)
        product_1.find_elements_by_tag_name("a")[-1].click()
        product_2 = elem[1]
        product_2.click()
        product_2.click()
        time.sleep(4)
        product_2.find_elements_by_tag_name("a")[-1].click()
        product_3 = elem[2]
        product_3.click()
        product_3.click()
        time.sleep(4)
        product_3.find_elements_by_tag_name("a")[-1].click()
        time.sleep(2)
        driver.get(self.base_url+"/Product/Cart")
        time.sleep(3)
        input_qty = driver.find_element_by_name("quantity")
        for i in range(len(input_qty.get_attribute("value"))):
            input_qty.send_keys(Keys.BACKSPACE)
        input_qty.send_keys(5)
        driver.find_element_by_class_name("list-unstyled").click()
        time.sleep(3)
        driver.find_element_by_class_name("delete-item").click()
        time.sleep(2)
        driver.find_element_by_link_text("はい").click()
        time.sleep(5)
        
        add_another_product = driver.find_element_by_link_text("他の製品を追加する").click()
        time.sleep(3)
        driver.find_element_by_link_text("追加 +").click()
        time.sleep(4)
        driver.find_element_by_class_name("ss-single-button").click()
        time.sleep(4)

        driver.get(self.base_url+"/Product/Cart")
        time.sleep(4)
        driver.find_elements_by_link_text("レジに進む")[-1].click()
        self.success = True
        time.sleep(10)
    

    def success_output(self):
        return  '''
                add product to cart - PASS
                change quantity - PASS
                delete product - PASS
                add another product - PASS
                click cart checkout button - PASS
                '''
    
    def errorr_output(self):
        return  '''
                Add product to cart - FAIL
                Change quantity - FAIL
                Delete product - FAIL
                Add another product - FAIL
                Click cart checkout button - FAIL
                '''

if __name__ == '__main__':
    unittest.main()