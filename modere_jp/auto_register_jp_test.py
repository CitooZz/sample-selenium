# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from base_handler import BaseTest
import unittest
import string
import random


class RegisterTest(BaseTest):

    def test_register(self):
        self.email = ''.join(random.choice(string.ascii_lowercase) for _ in range(6)) + '@ixod.com'
        driver = self.driver
        driver.maximize_window()
        driver.get(self.base_url + "/Account/#/CreateAccount")
        driver.find_element_by_name("email").send_keys(self.email)
        driver.find_element_by_name("verifyEmail").send_keys(self.email)
        driver.find_element_by_name("lastName").send_keys("First Name")
        driver.find_element_by_name("firstName").send_keys("Last Name")
        driver.find_element_by_name("password").send_keys("radagila21")
        driver.find_element_by_name("verifyPassword").send_keys("radagila21")

        #select birthday
        Select(driver.find_element_by_name("birthYear")).select_by_value("1985")
        Select(driver.find_element_by_name("birthMonth")).select_by_value("03")
        Select(driver.find_element_by_name("birthDay")).select_by_value("10")

        driver.find_element_by_name("agreeToTerms").click()
        driver.find_element_by_class_name("button").click()

        
        #river.find_element_by_link_text("ショッピング").click()
        driver.get(self.base_url + "/Category/personal-care")
        self.success = True

    def tearDown(self):
        self.driver.close()

    def success_output(self):
        return  "Register - PASS"
    
    def errorr_output(self):
        return  "Register - FAIL"


if __name__ == "__main__":
	unittest.main()