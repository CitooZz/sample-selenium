# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from base_handler import BaseTest
import unittest
import time


class ForgetPassword(BaseTest):

    def tearDown(self):
        self.driver.close()

    def test_forget_password(self):
        driver = self.driver
        self.open_page_forget_password(driver)
        self.send_password_reset(driver)
        time.sleep(10)

    def open_page_forget_password(self, driver):
        driver = self.driver
        driver.get(self.base_url+"/Account/#/Login")
        button_forget = driver.find_elements_by_link_text("パスワードをお忘れですか?")
        button_forget[0].click()
    
    def send_password_reset(self, driver): 
        driver = self.driver
        input_email = driver.find_elements_by_name("email")
        input_email = input_email[0]
        input_email.send_keys("jensen@ixod.com")
        input_email.send_keys(Keys.ENTER)


if __name__ == "__main__":
    unittest.main()