from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from base_handler import BaseTest
import unittest
import time
import sys


class LoginLogout(BaseTest):

    def test_login_logout(self):
        driver = self.driver
        driver.get(self.base_url)
        self.login()
        time.sleep(5)
        self.logout()
        self.success = True
    
    def success_output(self):
        return  "Test Login and Logout - PASS"
    
    def errorr_output(self):
        return  "Test Login and Logout - FAIL"


if __name__ == '__main__':
    system = sys.argv[0]
    unittest.main()
