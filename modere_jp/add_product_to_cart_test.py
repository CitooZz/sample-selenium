# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from base_handler import BaseTest
import unittest
import time


class AddProductToCart(BaseTest):

    def test_add_product_to_cart(self):
        driver = self.driver
        driver.get(self.base_url)
        self.goto_category("パーソナルケア")
        self.select_product(action="add_to_cart")
        self.success = True
        time.sleep(5)
    
    def success_output(self):
        return  "Add product to cart - PASS"
    
    def errorr_output(self):
        return  "Add product to cart - FAIL"

if __name__ == '__main__':
    unittest.main()