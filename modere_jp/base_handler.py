# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
import selenium.webdriver.support.ui as ui
from selenium.webdriver.common.by import By
import selenium.webdriver.support.expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import random
import unittest
import time
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(__file__)))
import config


class BaseTest(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()
        self.base_url = 'https://www.modere.co.jp'
        self.driver.implicitly_wait(30)
        self.success = None

    def tearDown(self):
        if self.success:
            print self.success_output()
        else:
            print self.errorr_output()
        self.driver.quit()

    def select_system(self, system):
        if system == 'dev':
            self.base_url = 'https://webtest.modere.com'

    def login(self):
        driver = self.driver
        driver.get(self.base_url)
        click_login = driver.find_element_by_link_text("ログイン").click()
        email = driver.find_element_by_name(
            "email").send_keys("havizvaisal@gmail.com")
        driver.find_element_by_name("password").send_keys("radagila21")
        driver.find_element_by_class_name('button').click()
        return driver

    def logout(self):
        driver = self.driver
        driver.find_element_by_class_name("dropdown").click()
        driver.find_element_by_class_name('icon-exit').click()

    def goto_category(self, category_name):
        '''
        example: "パーソナルケア"
        '''
        driver = self.driver
        driver.find_element_by_partial_link_text(category_name).click()

    def select_product(self, action="detail_page"):
        '''
        randomly select product based on category list page
        '''
        driver = self.driver
        #ui.WebDriverWait(driver, 30).until(EC.visibility_of_element_located((By.CLASS_NAME, "product-image")))
        elem = driver.find_elements_by_class_name("product-image")
        product = elem[1]
        ActionChains(driver).move_to_element(product).perform()
        time.sleep(5)
        if action == "detail_page":
            product_link = product.find_element_by_tag_name("a")
            product_link.click()
            time.sleep(5)
        elif action == "add_to_cart":
            add_to_cart_button = product.find_elements_by_tag_name("a")[-1]
            add_to_cart_button.click()

    def action_detail_product_page(self, action="add_to_cart"):
        driver = self.driver
        select = Select(driver.find_element_by_name("productQuantity"))
        driver.find_element_by_class_name("onetime-toggle").click()
        if action == 'add_to_cart':
            select.select_by_value("2")
            driver.find_element_by_id("btnAddProductToCart").click()
        elif action == 'add_to_my_favorite':
            driver.find_element_by_link_text("お気に入り").click()
        elif action == 'add_to_recomended_list':
            driver.find_element_by_link_text("おすすめリスト").click()
        elif action == 'share_to_email':
            driver.find_element_by_link_text("シェア").click()
            driver.find_element_by_link_text("Email").click()
            driver.find_element_by_name(
                "shareEmailRecipient").send_keys("Jensen")
            driver.find_element_by_name("shareEmailName").send_keys("Jewe")
            driver.find_element_by_name(
                "shareEmailEmails").send_keys("jensen@biznet3.com")
            driver.find_elements_by_tag_name("button")[-2].click()
            time.sleep(5)
        elif action == 'regular_delivery':
            driver.find_element_by_class_name("smartship-toggle").click()
            time.sleep(5)
            driver.find_elements_by_id("btnAddProductToCart")[-1].click()
            time.sleep(5)
        elif action == "zoom_img":
            zoom_img = driver.find_element_by_class_name("zoomImg")
            zoom_img.click()
            time.sleep(2)
            zoom_img.click()
        elif action == "product_tab":
            tab = driver.find_element_by_class_name("tab-list")
            tab.find_elements_by_tag_name("li")[0].click()
            time.sleep(1)
            tab.find_elements_by_tag_name("li")[1].click()
            time.sleep(1)
            tab.find_elements_by_tag_name("li")[2].click()
